/**
 * Created by arch on 6/2/17.
 */

public class FluentCl {

    public static int staticField;

    FluentCl doSmth1 (){
        System.out.println("doSmth1 " + staticField);
        return this;
    }

    FluentCl doSmth2 (){
        System.out.println("doSmth2 " + staticField);
        return this;
    }

}
