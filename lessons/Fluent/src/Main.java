/**
 * Created by arch on 6/2/17.
 */
public class Main {

    public static void main(String[] args) {

        FluentCl fluent1 = new FluentCl();
        FluentCl fluent2 = new FluentCl();

        fluent1.doSmth1().doSmth2();
        fluent2.doSmth1().doSmth2();

        FluentCl.staticField = 777;

        fluent1.doSmth1().doSmth2();
        fluent2.doSmth1().doSmth2();

    }
}
