package lesson8;

import java.util.LinkedList;
import java.util.Stack;

/**
 * Created by arch on 6/5/17.
 */
public class OurStack<T> {

    private LinkedList<T> ll = new LinkedList<>();

    public boolean empty() {

        return ll.isEmpty();
    }

    public T peek( ) {

        return ll.peek()
    }

    public T pop( ) {

        return ll.pop();
    }

    public void push (T element){

        ll.push(element);
    }

    public boolean search () {

        return ll.contains();
    }
}













