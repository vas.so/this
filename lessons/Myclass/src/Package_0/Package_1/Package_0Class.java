package Package_0.Package_1;

/**
 * Created by arch on 5/18/17.
 */
// A наслідує B
public class Package_0Class extends Package_1Class {

              String fieldIn0          = "internal field";  // package level
    public    String publicFieldIn0    = "public field";    //
    protected String protectedFieldIn0 = "protected field"; //
    private   String privateFieldIn0   = "private string";  //

    public void getProtectedFieldIn1 () {
        System.out.println(protectedFieldIn1);
        System.out.println(fieldIn1);
        System.out.println(this.publicFieldIn1);
        return;
    }
}
